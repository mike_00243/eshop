import { Component, OnInit } from '@angular/core';
import { Item } from '../model/item.entity';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items: Item[];
  total = 0;

  constructor(private activatedRoute: ActivatedRoute, private productSerive: ProductService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      var id = params['id'];

      if (id) {
        console.log('id : ' + params.id);
        this.productSerive.find(id).subscribe((item) => {
          var myItem: Item = {
            product: item,
            quantity: 1
          };
          if (localStorage.getItem('cart') == null) {
            let cart: any[];
            cart.push(JSON.stringify(myItem));
            localStorage.setItem('cart', JSON.stringify(cart));
          } else {
            let cart: any = JSON.parse(localStorage.getItem('cart'));
            let index: number = -1;
            for (let i = 0; i < cart.length; i++) {
              let item: Item = JSON.parse(cart[i]);
              if (item.product.id == id) {
                index = i;
                break;
              }
              if (index === -1) {
                cart.push(JSON.stringify(myItem));
                localStorage.setItem('cart', JSON.stringify(cart));
              } else {
                let item: Item = JSON.parse(cart[index]);
                item.quantity += 1;
                cart[index] = JSON.stringify(item);
                localStorage.setItem('cart', JSON.stringify(cart));
              }
            }
            this.loadCart();
          }
        });
      } else {
        this.loadCart();
      }
    });
  }

  loadCart() {
    this.total = 0;
    this.items = [];
    let cart = JSON.parse(localStorage.getItem('cart'));
    for (let i = 0; i < cart.length; i++) {
      let item: Item = JSON.parse(cart[i]);
      this.items.push({
        product: item.product,
        quantity: item.quantity
      });
      this.total += item.product.price * item.quantity;
    }
  }

  remove(id: string) {
    let cart = JSON.parse(localStorage.getItem('cart'));
    let index: number = -1;
    for (let i = 0; i < cart.length; i++) {
      let item: Item = JSON.parse(cart[i]);
      if (item.product.id === id) {
        cart.splice(i, 1);
        break;
      }
      localStorage.setItem('cart', JSON.stringify(cart));
      this.loadCart();
    }
  }

}
