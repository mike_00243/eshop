import { Injectable } from '@angular/core';
import { Product } from '../model/product.entity';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[];
  _url: string = 'http://localhost:3000/products';

  constructor(private http: HttpClient) {
    this.products = [];
  }

  findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this._url);
  }

  find(id: string): Observable<Product> {
    return this.http.get<Product>(this._url + '/' + id);
  }

  private getSelectedIndex(id: string) {
    for (let index = 0; index < this.products.length; index++) {
      if (this.products[index].id === id) {
        return index;
      }
    }
    return -1;
  }
}
